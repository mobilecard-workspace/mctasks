package com.addcel.mctasks.mybatis.model.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.addcel.mctasks.client.h2hbanorte.model.AccountRecord;
import com.addcel.mctasks.mybatis.model.vo.Establecimiento;
import com.addcel.mctasks.mybatis.model.vo.ProjectMC;
import com.addcel.mctasks.mybatis.model.vo.TransactionLCPF;
import com.addcel.mctasks.mybatis.model.vo.TransactionQUARTZ;





public interface ServiceMapper {
	
	public List<TransactionLCPF> getTransactionLCPF( @Param(value = "fecha") String fecha);
	public Establecimiento getEstablecimiento( @Param(value = "id_est") long id_est);
	public AccountRecord getAccount(@Param(value = "idaccount") long idaccount);
	public long getMaxIdBitacora();
	public void insertTransactionQuartz(TransactionQUARTZ transaction);
	public void updateBitacoraBanorte(@Param(value = "trans_number") long trans_number,@Param(value = "b_reference") String b_reference,@Param(value = "id_est") long id_est,@Param(value = "fecha") String fecha);
	public String getParameter(@Param(value = "parameter") String parameter);
	public ProjectMC getProjectMC( @Param(value = "cont") String cont,@Param(value = "recu") String recu);
	
}
