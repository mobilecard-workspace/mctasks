package com.addcel.mctasks.mybatis.model.vo;

import java.util.Date;

public class TransactionQUARTZ {
	
	private long id;
	private long id_establecimiento;
	private String id_transaccion;
	private long id_cuenta;
	private String referencia_h2h;
	double monto;
	private int estatus;
	private Date fecha_corte; 
	private Date fecha_transaccion;
	
	public TransactionQUARTZ() {
		// TODO Auto-generated constructor stub
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId_establecimiento() {
		return id_establecimiento;
	}

	public void setId_establecimiento(long id_establecimiento) {
		this.id_establecimiento = id_establecimiento;
	}

	public String getId_transaccion() {
		return id_transaccion;
	}

	public void setId_transaccion(String id_transaccion) {
		this.id_transaccion = id_transaccion;
	}

	public long getId_cuenta() {
		return id_cuenta;
	}

	public void setId_cuenta(long id_cuenta) {
		this.id_cuenta = id_cuenta;
	}

	public String getReferencia_h2h() {
		return referencia_h2h;
	}

	public void setReferencia_h2h(String referencia_h2h) {
		this.referencia_h2h = referencia_h2h;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}

	public Date getFecha_corte() {
		return fecha_corte;
	}

	public void setFecha_corte(Date fecha_corte) {
		this.fecha_corte = fecha_corte;
	}

	public Date getFecha_transaccion() {
		return fecha_transaccion;
	}

	public void setFecha_transaccion(Date fecha_transaccion) {
		this.fecha_transaccion = fecha_transaccion;
	}
	
	


}
