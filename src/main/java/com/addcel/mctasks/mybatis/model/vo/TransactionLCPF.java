package com.addcel.mctasks.mybatis.model.vo;

public class TransactionLCPF {
	
	private double total;
	private long id_establecimiento;
	private long transacciones;

	public TransactionLCPF() {
		// TODO Auto-generated constructor stub
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public long getId_establecimiento() {
		return id_establecimiento;
	}

	public void setId_establecimiento(long id_establecimiento) {
		this.id_establecimiento = id_establecimiento;
	}

	public long getTransacciones() {
		return transacciones;
	}

	public void setTransacciones(long transacciones) {
		this.transacciones = transacciones;
	}
	
	
	
}
