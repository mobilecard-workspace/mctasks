package com.addcel.mctasks.mybatis.model.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Establecimiento {
	
	private long id;
	private String usuario;
	private String pass;
	private String nombre_establecimiento;
	private String rfc;
	private String razon_social;
	private String representante_legal;
	private String direccion_establecimiento;
	private String email_contacto;
	private String telefono_contacto;
	private int id_banco;
	private String cuenta_clabe;
	private String nombre_prop_cuenta;
	private long id_account;
	private int estatus;
	private double comision_fija; 
	private double comision_porcentaje;
	private String urlLogo;
	private int tipo_comision;
	private String cuenta_tipo; 
	private String codigo_banco;
	
	public Establecimiento() {
		// TODO Auto-generated constructor stub
	}
	
	
	public String getCuenta_tipo() {
		return cuenta_tipo;
	}



	public void setCuenta_tipo(String cuenta_tipo) {
		this.cuenta_tipo = cuenta_tipo;
	}



	public String getCodigo_banco() {
		return codigo_banco;
	}



	public void setCodigo_banco(String codigo_banco) {
		this.codigo_banco = codigo_banco;
	}



	public void setTipo_comision(int tipo_comision) {
		this.tipo_comision = tipo_comision;
	}
	
	public int getTipo_comision() {
		return tipo_comision;
	}
	
	public void setUrlLogo(String urlLogo) {
		this.urlLogo = urlLogo;
	}
	
	public String getUrlLogo() {
		return urlLogo;
	}

	public double getComision_fija() {
		return comision_fija;
	}



	public void setComision_fija(double comision_fija) {
		this.comision_fija = comision_fija;
	}



	public double getComision_porcentaje() {
		return comision_porcentaje;
	}



	public void setComision_porcentaje(double comision_porcentaje) {
		this.comision_porcentaje = comision_porcentaje;
	}



	public void setId_account(long id_account) {
		this.id_account = id_account;
	}
	
	public long getId_account() {
		return id_account;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getNombre_establecimiento() {
		return nombre_establecimiento;
	}

	public void setNombre_establecimiento(String nombre_establecimiento) {
		this.nombre_establecimiento = nombre_establecimiento;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getRazon_social() {
		return razon_social;
	}

	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}

	public String getRepresentante_legal() {
		return representante_legal;
	}

	public void setRepresentante_legal(String representante_legal) {
		this.representante_legal = representante_legal;
	}

	public String getDireccion_establecimiento() {
		return direccion_establecimiento;
	}

	public void setDireccion_establecimiento(String direccion_establecimiento) {
		this.direccion_establecimiento = direccion_establecimiento;
	}

	public String getEmail_contacto() {
		return email_contacto;
	}

	public void setEmail_contacto(String email_contacto) {
		this.email_contacto = email_contacto;
	}

	public String getTelefono_contacto() {
		return telefono_contacto;
	}

	public void setTelefono_contacto(String telefono_contacto) {
		this.telefono_contacto = telefono_contacto;
	}

	public int getId_banco() {
		return id_banco;
	}

	public void setId_banco(int id_banco) {
		this.id_banco = id_banco;
	}

	public String getCuenta_clabe() {
		return cuenta_clabe;
	}

	public void setCuenta_clabe(String cuenta_clabe) {
		this.cuenta_clabe = cuenta_clabe;
	}

	public String getNombre_prop_cuenta() {
		return nombre_prop_cuenta;
	}

	public void setNombre_prop_cuenta(String nombre_prop_cuenta) {
		this.nombre_prop_cuenta = nombre_prop_cuenta;
	}

	public int getEstatus() {
		return estatus;
	}

	public void setEstatus(int estatus) {
		this.estatus = estatus;
	}
	
	

}
