package com.addcel.mctasks.spring.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mctasks.client.h2hbanorte.model.AccountRecord;
import com.addcel.mctasks.client.h2hbanorte.model.EnqueuePaymentRequest;
import com.addcel.mctasks.client.h2hbanorte.model.EnqueueTransactionResponse;
import com.addcel.mctasks.mybatis.model.mapper.ServiceMapper;
import com.addcel.mctasks.mybatis.model.vo.Establecimiento;
import com.addcel.mctasks.mybatis.model.vo.ProjectMC;
import com.addcel.mctasks.mybatis.model.vo.TransactionLCPF;
import com.addcel.mctasks.mybatis.model.vo.TransactionQUARTZ;
import com.addcel.mctasks.utils.Business;
import com.google.gson.Gson;
import com.sun.org.apache.bcel.internal.generic.ACONST_NULL;

@Service
public class MCTasksService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MCTasksService.class);
	
	@Autowired
	ServiceMapper mapper;
	
	private Gson gson = new Gson();
	private H2HCLIENT h2hclient = new H2HCLIENT();
	SimpleDateFormat DFormat = new SimpleDateFormat("yyyy-MM-dd"); //2018-02-07  
	
	
	public void TransferH2HBanorte(Date fecha){
		boolean send = false;
		List<TransactionLCPF> transactions = mapper.getTransactionLCPF(DFormat.format(fecha)); //
		int size = transactions.size();
		if(transactions != null && size > 0){
			
			for(int i=0; i<size; i++){
				LOGGER.debug("Item: "+ gson.toJson(transactions.get(i)));
				LOGGER.debug("Procesando Cliente " + transactions.get(i).getId_establecimiento());
				/***********************************hack solo enciar el 70% del monto*/
				/*try{
					String porcentaje = mapper.getParameter("@LCPF_PORCENTAJE_ENVIO");
					if(porcentaje.equals("1")){
						double nuevoTotal = transactions.get(i).getTotal()*0.70;
						LOGGER.debug("Nuevo monto calculado " + transactions.get(i).getTotal() +" --- "+ nuevoTotal);
						transactions.get(i).setTotal(nuevoTotal);
						
					}
				}catch(Exception ex){
					LOGGER.error("ERROR AL OBTENER BANDERA DE PORCENTAJE ENVIO", ex);
				}*/
				/****************************************************** */
				send = EnqueuePayment(transactions.get(i).getId_establecimiento(), transactions.get(i).getTotal(), fecha, "es");
				if(send)
					LOGGER.debug("[TRANSFERENCIA CORRECTAMENTE ENVIADA] " + fecha + "-" + transactions.get(i).getId_establecimiento());
				else
					LOGGER.debug("[ERROR AL ENVIAR TRANSFERENCIA] "+ fecha + "-" + transactions.get(i).getId_establecimiento());
			}
		}else{
			LOGGER.debug("NO SE ENCONTRARON TRANSACCIONES PARA PROCESAR [LA CUENTA POR FAVOR] "+ fecha);
		}
		
	}
	
	/*
	 * Encola una transacción para transferencia
	 * Se compara RFC,NUM CUENTA, COD BANCO, TIPO CUENTA para determinar que estén registradas
	 */
	private boolean EnqueuePayment(long id_establecimiento, double amount, Date fecha_corte, String idioma){
		EnqueuePaymentRequest payment = new EnqueuePaymentRequest();
		EnqueueTransactionResponse response = new EnqueueTransactionResponse();
		TransactionQUARTZ transaction = new TransactionQUARTZ();
		try{
			AccountRecord fromAccount = new AccountRecord();
			
			fromAccount.setActNumber("0276948693"); 
			fromAccount.setActType("ACT"); 
			fromAccount.setBankCode("072"); 
			fromAccount.setContact("Jorge Silva Gallegos"); 
			fromAccount.setEmail("jorge@mobilecardmx.com"); 
			fromAccount.setHolderName("ADCEL SA DE CV"); 
			fromAccount.setPhone("5555403124"); 
			fromAccount.setRfc("ADC090715DW3");
			
			Establecimiento est = mapper.getEstablecimiento(id_establecimiento);
			long accountId = est.getId_account();
			
			AccountRecord toAccount = mapper.getAccount(accountId);
			
			if(toAccount == null){
				toAccount = new AccountRecord();
				toAccount.setActNumber(est.getCuenta_clabe());
				toAccount.setActType(est.getCuenta_tipo());
				toAccount.setBankCode(est.getCodigo_banco());
				toAccount.setContact(est.getNombre_prop_cuenta());
				toAccount.setEmail(est.getEmail_contacto());
				toAccount.setHolderName(est.getNombre_establecimiento());
				toAccount.setPhone(est.getTelefono_contacto() == null ? "5555403124" : est.getTelefono_contacto().isEmpty() ? "5555403124" : est.getTelefono_contacto());
				toAccount.setRfc(est.getRfc());
			}
			
			
			LOGGER.debug("To Account: " + accountId + " " + gson.toJson(toAccount));
			//LOGGER.debug("From Account: " + fromAccount);
			
			/*
			1 - Cobrar al negocio
			2 - CObrar al usuario
			3.- Cobrar a ambos
			*/
			LOGGER.debug("Tipo de comision " + est.getTipo_comision());
			if(est.getTipo_comision() == 1){
				LOGGER.debug("La comision se le cobrara al comercio...");
				double totalComision = amount * est.getComision_porcentaje();
				amount = amount - (totalComision+est.getComision_fija());
			}
			
			String id_rerefence = mapper.getMaxIdBitacora()+toAccount.getHolderName().substring(0,3)+new Date().getSeconds();
			payment.setAmount(amount);
			payment.setClientReference(id_rerefence); //TODO
			payment.setDescription("LA CUENTA POR FAVOR");
			payment.setFromAccount(fromAccount);
			payment.setToAccount(toAccount);
			//payment.setToken(login.getToken());
			response = h2hclient.EnqueuePayment(payment,idioma);
			
		//	LOGGER.debug("[RESPUESTA PAGO] " + gson.toJson(response));
			
			if(response.getResult().isSuccess()){
				//String responseBD = mapper.H2HBanorte_Business(paymentRequest.getIdUser(), bitacora.getIdBitacora(), Long.parseLong(response.getTransactionReference()), "", 1, "es");
				LOGGER.debug("[Transaccion guardada] Exito " + response.getTransactionReference());
				//mapper.updateTbitacora(idBitacora,1,paymentRequest.getAuthProcom());
				transaction.setEstatus(2); //encolado en Banorte
				transaction.setId_transaccion(response.getTransactionReference()); // referencia retornada por banorte
				//actualizando transacciones en bitacora banorte
				LOGGER.debug("[ACTUALIZANDO BITACORA BANOERTE]");
				mapper.updateBitacoraBanorte(Long.parseLong(response.getTransactionReference()), response.getTransactionReference(), id_establecimiento, DFormat.format(fecha_corte));
				
				HashMap< String, String> datos = new HashMap<String, String>();
				datos.put("NOMBRE", est.getNombre_establecimiento());
				datos.put("FECHA",DFormat.format(fecha_corte));
				datos.put("REFERENCIA",response.getTransactionReference());
				datos.put("IMPORTE", amount+"");
				LOGGER.debug("AA: " + amount+"");
				ProjectMC project = mapper.getProjectMC("MailSenderAddcel", "enviaCorreoAddcel");//mapper.getProjectMC("GestionUsuarios", "userMCActivate");
				Business.SendMailMicrosoft(est.getEmail_contacto(), mapper.getParameter("@MESSAGE_LCPF_COMERCIO_ES"), "¡LA CUENTA POR FAVOR!", datos,project.getUrl());
			}
			else
			{
				LOGGER.debug("[Transaccion guardada] error al cobrar");
				transaction.setEstatus(3); // error al encolar en banorte
				transaction.setId_transaccion(""); // referencia retornada por banorte
				//mapper.updateTbitacora(idBitacora,2,"");
				
			}
			
			
			
			transaction.setFecha_corte(fecha_corte);
			transaction.setFecha_transaccion(new Date());
			transaction.setId_cuenta(accountId);
			transaction.setId_establecimiento(id_establecimiento);
			transaction.setMonto(amount);
			transaction.setReferencia_h2h(id_rerefence);
			mapper.insertTransactionQuartz(transaction);
			
			return true;
			
		}catch(Exception ex){
			//LOGGER.error("[ERROR AL ENCOLAR TRANSFERENCIA]  " +  gson.toJson(paymentRequest));
			LOGGER.error("[ERROR AL ENCOLAR PAGO] " , ex);
			return false;//McResponse;
		}
	}
	
}
