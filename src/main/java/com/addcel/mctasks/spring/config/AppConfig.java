package com.addcel.mctasks.spring.config;


import java.util.HashMap;
import java.util.Map;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import com.addcel.mctasks.spring.job.MyJobTwo;
import com.addcel.mctasks.spring.service.MCTasksService;

import javax.sql.DataSource;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.addcel.mctasks.spring")
@MapperScan("com.addcel.mctasks.mybatis.model.mapper")
@PropertySource("classpath:properties/datasource.properties")
public class AppConfig extends WebMvcConfigurerAdapter{

	public static final X509HostnameVerifier ALLOW_ALL_HOSTNAME_VERIFIER     = new AllowAllHostnameVerifier();
	
	
	/**
	 * Valores recuperados de datasource.properties
	 */
	@Value( "${jdbc.url}" ) private String jdbcUrl;
    @Value( "${jdbc.driverClassName}" ) private String driverClassName;
    @Value( "${jdbc.username}" ) private String username;
    @Value( "${jdbc.password}" ) private String password;
    
	
     /**
      * Crear Datasource para la conexion
      * @return DataSource
      */
	 public DataSource dataSource() {
       SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
        dataSource.setDriverClass(com.mysql.jdbc.Driver.class);
        dataSource.setUsername(username);
        dataSource.setUrl(jdbcUrl);
        dataSource.setPassword(password);
       
		/*PooledDataSource dataSource = new PooledDataSource();
        dataSource.setDriver(driverClassName);
        dataSource.setUsername(username);
        dataSource.setUrl(jdbcUrl);
        dataSource.setPassword(password);
        
        dataSource.setPoolMaximumActiveConnections(10);
        dataSource.setPoolMaximumIdleConnections(5);
        dataSource.setPoolMaximumCheckoutTime(100);
        dataSource.setPoolPingQuery("select 1");*/
        
        return dataSource;
    }
	 
   
    
	
	  /**
	   *  Crear DataSourceTransactionManager a partir del datasource
	   * @return DataSourceTransactionManager
	   */
	  @Bean
       public DataSourceTransactionManager transactionManager()
	    {
	        return new DataSourceTransactionManager(dataSource());
	    }
	     /**
	      * Crear sessionFactory
	      * @return SqlSessionFactoryBean
	      * @throws Exception
	      */
	    @Bean
	    public SqlSessionFactoryBean sqlSessionFactoryBean() throws Exception{
	        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
	        sessionFactory.setDataSource(dataSource());        
	        // mybatis mapper
	        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
	        sessionFactory.setTypeAliasesPackage("com.addcel.mctasks.mybatis.model.vo");
	        
	        return sessionFactory;
	    }
	    
	    
	    @Bean
	    public InternalResourceViewResolver jspViewResolver() {
	        InternalResourceViewResolver bean = new InternalResourceViewResolver();
	        bean.setPrefix("/WEB-INF/views/");
	        bean.setSuffix(".jsp");
	        return bean;
	    }
	    
	 
	    
	    //CONFIGURACION DE QUARTZ
	    @Bean
		public MethodInvokingJobDetailFactoryBean methodInvokingJobDetailFactoryBean() {
			MethodInvokingJobDetailFactoryBean obj = new MethodInvokingJobDetailFactoryBean();
			obj.setTargetBeanName("jobone");
			obj.setTargetMethod("myTask");
			return obj;
		}
	    
	    @Bean
	    public SimpleTriggerFactoryBean simpleTriggerFactoryBean(){
	    	SimpleTriggerFactoryBean stFactory = new SimpleTriggerFactoryBean();
	    	stFactory.setJobDetail(methodInvokingJobDetailFactoryBean().getObject());
	    	stFactory.setStartDelay(3000);
	    	stFactory.setRepeatInterval(30000 * 3);
	    	stFactory.setRepeatCount(3);
	    	
	    	return stFactory;
	    } 
	    
	    @Bean
	    public JobDetailFactoryBean jobDetailFactoryBean(){
	    	JobDetailFactoryBean factory = new JobDetailFactoryBean();
	    	factory.setJobClass(MyJobTwo.class);
	    	Map<String,Object> map = new HashMap<String,Object>();
	    	map.put("name", "RAM");
	    	map.put(MyJobTwo.COUNT, 1);
	    	map.put("service", new MCTasksService());
	    	factory.setJobDataAsMap(map);
	    	factory.setGroup("mygroup");
	    	factory.setName("myjob");
	    	
	    	return factory;
	    } 
	    
	    @Bean
	    public CronTriggerFactoryBean cronTriggerFactoryBean(){
	    	CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
	    	//stFactory.setJobDetail(jobDetailFactoryBean().getObject());
	    	stFactory.setJobDetail(methodInvokingJobDetailFactoryBean().getObject());
	    	stFactory.setStartDelay(3000);
	    	stFactory.setName("mytrigger");
	    	stFactory.setGroup("mygroup");
	    	stFactory.setCronExpression("0 10 3 * * ? *"); //0 10 2 * * ? *
	    	//
	    	return stFactory;
	    } 
	    
	    @Bean
	    public SchedulerFactoryBean schedulerFactoryBean() {
	    	SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
	    	//scheduler.setTriggers(simpleTriggerFactoryBean().getObject(),cronTriggerFactoryBean().getObject());
	    	scheduler.setTriggers(cronTriggerFactoryBean().getObject());
	    	return scheduler;
	    } 
	    
}
