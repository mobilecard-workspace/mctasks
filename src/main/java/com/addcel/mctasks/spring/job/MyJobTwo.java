package com.addcel.mctasks.spring.job;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.addcel.mctasks.spring.service.MCTasksService;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class MyJobTwo extends QuartzJobBean {
	 private static final Logger LOGGER = LoggerFactory.getLogger(MyJobTwo.class);
	
	public static final String COUNT = "count";
	private String name;
	//private MCTasksService service;
	
	// @Autowired
	// MCTasksService service;
	
  protected void executeInternal(JobExecutionContext ctx) throws JobExecutionException {
    	   JobDataMap dataMap = ctx.getJobDetail().getJobDataMap();
    	   int cnt = dataMap.getInt(COUNT);
	   JobKey jobKey = ctx.getJobDetail().getKey();
	   System.out.println(jobKey+": "+name+": "+ cnt);
	   cnt++;
	   dataMap.put(COUNT, cnt);
        
	   
	 /*  LOGGER.debug("INICIANDO CRON");
   	Date fecha = new Date();
   	LocalDate date = fecha.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
   	LocalDate dateResultado = date.minus(1, ChronoUnit.DAYS);
   	fecha = Date.from(dateResultado.atStartOfDay(ZoneId.systemDefault()).toInstant());
   	LOGGER.debug("Iniciando proceso corte " + fecha);
    //MCTasksService service = new MCTasksService();
   	service.TransferH2HBanorte(fecha);*/
	   
       }
        
  /*public void setService(MCTasksService service) {
	this.service = service;
}*/
  
	public void setName(String name) {
		this.name = name;
	}
} 