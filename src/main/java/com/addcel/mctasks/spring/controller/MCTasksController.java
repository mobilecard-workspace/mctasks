package com.addcel.mctasks.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class MCTasksController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MCTasksController.class);
	
	@RequestMapping(value = "/test",method = RequestMethod.GET)
	public ModelAndView ejemplosend(){
		ModelAndView view = new ModelAndView("index");
		LOGGER.debug("Escribiendo en log...");
		return view;
	}

}
